package com.bitcointoolkit.common.cash.param;

import com.bitcointoolkit.common.cash.AddressType;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author can
 *         2018/11/1
 */
public class TestNetChainParams extends AbstractChainParams {
    public static final TestNetChainParams PARAMS = new TestNetChainParams();

    public TestNetChainParams() {
        this.base58Prefixes = new HashMap<>();
        base58Prefixes.put(AddressType.PUBKEY_ADDRESS, Arrays.asList((byte)0x6F));
        base58Prefixes.put(AddressType.SCRIPT_ADDRESS, Arrays.asList((byte)0xC4));
    }
}
