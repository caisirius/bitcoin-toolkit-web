package com.bitcointoolkit.common.cash.param;

import com.bitcointoolkit.common.cash.AddressType;

import java.util.List;
import java.util.Map;

/**
 * @author can
 *         2018/11/1
 */
public abstract class AbstractChainParams {

    protected Map<AddressType, List<Byte>> base58Prefixes;

    public byte[] base58Prefix(AddressType type) {
        List<Byte> prefix = base58Prefixes.get(type);
        byte[] ret = new byte[prefix.size()];
        for (int i=0;i<prefix.size(); i++) {
            ret[i] = prefix.get(i);
        }
        return ret;
    }
}
