package com.bitcointoolkit.common.cash.param;

import com.bitcointoolkit.common.cash.AddressType;

import java.util.Arrays;
import java.util.HashMap;

/**
 * FreeCash
 * @author can
 *         2018/11/1
 */
public class FcMainChainParams extends AbstractChainParams {
    public static final FcMainChainParams PARAMS = new FcMainChainParams();

    public FcMainChainParams() {
        this.base58Prefixes = new HashMap<>();
        base58Prefixes.put(AddressType.PUBKEY_ADDRESS, Arrays.asList((byte)0x68, (byte)0xfc, (byte)0xfc));
        base58Prefixes.put(AddressType.SCRIPT_ADDRESS, Arrays.asList((byte)5));
    }
}
