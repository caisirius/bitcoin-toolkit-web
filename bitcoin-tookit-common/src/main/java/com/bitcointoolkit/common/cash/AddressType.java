package com.bitcointoolkit.common.cash;

/**
 * [类描述]
 *
 * @author caican
 * @date 2018/6/29
 */
public enum AddressType {
    /**
     * 公钥地址
     */
    PUBKEY_ADDRESS,
    /**
     * 脚本地址/隔离见证地址
     */
    SCRIPT_ADDRESS,
//    SECRET_KEY, // dont know usage
//    EXT_PUBLIC_KEY,
//    EXT_SECRET_KEY,
}
