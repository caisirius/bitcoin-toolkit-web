package com.bitcointoolkit.common.cash; 

import com.bitcointoolkit.common.cash.param.FcMainChainParams;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* PubKey Tester. 
* 
* @author <Authors name> 
* @since <pre>六月 21, 2018</pre> 
* @version 1.0 
*/ 
public class PubKeyTest { 

    @Before
    public void before() throws Exception { 
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: size() 
    */ 
    @Test
    public void test1() throws Exception {
        String key = "026173240c95bd7e70c870b74419ee58c30f3edf47b3696ab8865c688291a89a9d";
        byte[] xx = EncodeUtil.toBytes(key);
        PubKey pubKey = new PubKey(xx);
        Assert.assertEquals("17n2iGPRxEyTTAAURCaUHum8JQcnQ3fjEM", pubKey.getAddress(true));
        Assert.assertEquals(33, pubKey.size());


    }
    @Test
    public void testFcPubKey() {
        String key = "026173240c95bd7e70c870b74419ee58c30f3edf47b3696ab8865c688291a89a9d";
        byte[] xx = EncodeUtil.toBytes(key);
        PubKey pubKey = new PubKey(xx, FcMainChainParams.PARAMS);

        System.out.println(pubKey.getAddress(true));
    }


} 
