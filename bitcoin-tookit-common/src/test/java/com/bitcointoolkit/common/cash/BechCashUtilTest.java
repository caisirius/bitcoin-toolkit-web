package com.bitcointoolkit.common.cash;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BechCashUtilTest {

    BechCashUtil CASH = BechCashUtil.getInstance();


    Map<String, String> testsSample = new HashMap<>();

    @Before
    public void before() {
        testsSample.put("1BpEi6DfDAUFd7GtittLSdBeYJvcoaVggu", "bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a");
        testsSample.put("1KXrWXciRDZUpQwQmuM1DbwsKDLYAYsVLR", "bitcoincash:qr95sy3j9xwd2ap32xkykttr4cvcu7as4y0qverfuy");
        testsSample.put("16w1D5WRVKJuZUsSRzdLp9w3YGcgoxDXb",  "bitcoincash:qqq3728yw0y47sqn6l2na30mcw6zm78dzqre909m2r");
        testsSample.put("3CWFddi6m4ndiGyKqzYvsFYagqDLPVMTzC", "bitcoincash:ppm2qsznhks23z7629mms6s4cwef74vcwvn0h829pq");
        testsSample.put("3LDsS579y7sruadqu11beEJoTjdFiFCdX4", "bitcoincash:pr95sy3j9xwd2ap32xkykttr4cvcu7as4yc93ky28e");
        testsSample.put("31nwvkZwyPdgzjBJZXfDmSWsC4ZLKpYyUw", "bitcoincash:pqq3728yw0y47sqn6l2na30mcw6zm78dzq5ucqzc37");

        testsSample.put("14Q9EfBp38WWJTS82HF2mpqsYg8RUnvfnp", "bitcoincash:qqj535d3vynxxmtavpan6uyy7592cecwcs4ndjucuj");


        testsSample.put("n1VAs96DPUmTXv1qmu6q3VhdCDSJFzQxKp", "bchtest:qrdsct2jtp24uxsd30tp39y4d09ph8m0wsez36rn7m");
        testsSample.put("mt2T2d4SNH5gp6ie1jSXa9vHivjm7R2njs", "bchtest:qzyntrh9qlz0qgcd6784x3lydllqgrrexqtx93tvz3");

    }
    @Test
    public void testEncodePayload() {
        String code = CASH.bechEncode(new byte[] {}, "prefix");
        assertEquals("prefix:x64nx6hz", code);
        code = CASH.bechEncode(new byte[] {}, "p");
        assertEquals("p:gpf8m4h7", code);
        byte[] res = CASH.bechDecode("qpzry9x8gf2tvdw0s3jn54khce6mua7l");
        code = CASH.bechEncode(res, "bitcoincash");
        assertEquals("bitcoincash:qpzry9x8gf2tvdw0s3jn54khce6mua7lcw20ayyn", code);
        res = CASH.bechDecode("555555555555555555555555555555555555555555555");
        code = CASH.bechEncode(res, "bchreg");
        assertEquals("bchreg:555555555555555555555555555555555555555555555udxmlmrz", code);
    }

    @Test
    public void testCashAddress2payload() {
        String org = "bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a";
        Entry<String, byte[]> entry = CASH.bechDecode(org, "");
        System.out.println(entry.getKey());
        System.out.println(Base58.encode(entry.getValue()));
        // System.out.println(entry.getValue());
        String encode = CASH.bechEncode(entry.getValue(), "bitcoincash");
        assertEquals(org, encode);
    }

    @Test
    public void testLegacy2payload() {
        for (Entry<String, String> entry : testsSample.entrySet()) {
            String legacy = entry.getKey();
            String cashaddr = entry.getValue();
            String result = CASH.transferLegacyToCashAddr(legacy);
            assertEquals(cashaddr, result);
        }
    }

    @Test
    public void testPayload2legacy() {
        byte[] data = new byte[] { 0, 4, 127, -120, 56, 88, 1, -8, -9 };
        byte[] target = CASH.payloadEncode(data, 0, data.length);
        byte[] res = CASH.payloadDecode(target, 0, target.length);
        assertEquals(new BitArray(data).toString(), new BitArray(res).toString());
    }

    @Test
    public void testPayload2legacy2() {
        for (Entry<String, String> entry : testsSample.entrySet()) {
            String legacy = entry.getKey();
            String cashaddr = entry.getValue();
            String result = CASH.transferCashAddrToLegacy(cashaddr);
            assertEquals(legacy, result);
        }
    }

    @Test
    public void testCheckLegacy() {
        for (Entry<String, String> entry : testsSample.entrySet()) {
            String legacy = entry.getKey();
            if (legacy.startsWith("1")) {
                CASH.checkLegacy(legacy);
            }
        }
        boolean checkErr = false;
        try {
            CASH.checkLegacy("1BpEi6DfDAUFd7GtittLSdBeYJvcoaVgg0");
        } catch (IllegalArgumentException e) {
            checkErr = true;
        }
        assertTrue(checkErr);

    }

    @Test
    public void testPubKeyHashToLegacy() {
        countPubKeyHashToLegacy("4a54e6d25321cc5447eabd61d54373022342ffc3",
                "17n2iGPRxEyTTAAURCaUHum8JQcnQ3fjEM");

        countPubKeyHashToLegacy("8df7e367b4e36b6d4ab5b34ed46e9f880bbe81c0",
                "1DwfEdVeWKpF6a7Stuuipu3MML8uwZD6vf");

        countPubKeyHashToLegacy("d4fe74020d87b01b07726f7dec925b8586b7e2ea",
                "1LRD5PEQmGihQ7HZWqnSAvSppGAzbXbcUQ");


        countScriptHashToLegacy("f815b036d9bbbce5e9f2a00abd1bf3dc91e95510",
                "3QJmV3qfvL9SuYo34YihAf3sRCW3qSinyC");
        countScriptHashToLegacy("afb497a46ba3baad44faefc6c8d85db2d64e2537",
                "3Hi4Wtccb5cTd7VvLpCyNqZ3p9ixYGAEcM");
        countScriptHashToLegacy("33023578f1740cb2aba3c573f09982eb437863bd",
                "36Lj2oW2Y4fZA7S3VgWFczkKbUkkeJgNQw");
    }
    private void countPubKeyHashToLegacy(String key, String address) {
        byte[] xx = EncodeUtil.toBytes(key);
        Assert.assertEquals(address, CASH.pubKeyHashToLegacy(xx));
    }
    private void countScriptHashToLegacy(String key, String address) {
        byte[] xx = EncodeUtil.toBytes(key);
        Assert.assertEquals(address, CASH.scriptHashToLegacy(xx));
    }

    /**
     * 从公钥计算 地址
     */
    @Test
    public void testPubKeyToLegacy() {
        //压缩的公钥
        countPubKeyToLegacy("026173240c95bd7e70c870b74419ee58c30f3edf47b3696ab8865c688291a89a9d",
                "17n2iGPRxEyTTAAURCaUHum8JQcnQ3fjEM");

        countPubKeyToLegacy("03764f4ce6881cb4b2cbdf26057e43a0a1cb1fd58f2d362785f712d35828135a46",
                "14Q9EfBp38WWJTS82HF2mpqsYg8RUnvfnp");

        // 未压缩的公钥
        countPubKeyToLegacy("0408ab2f56361f83064e4ce51acc291fb57c2cbcdb1d6562f6278c43a1406b548fd6cefc11bcc29eb620d5861cb9ed69dc39f2422f54b06a8af4f78c8276cfdc6b",
                "15ubjFzmWVvj3TqcpJ1bSsb8joJ6gF6dZa");

        countPubKeyToLegacy("04f5eeb2b10c944c6b9fbcfff94c35bdeecd93df977882babc7f3a2cf7f5c81d3b09a68db7f0e04f21de5d4230e75e6dbe7ad16eefe0d4325a62067dc6f369446a",
                "1BW18n7MfpU35q4MTBSk8pse3XzQF8XvzT");

    }
    private void countPubKeyToLegacy(String pubkey, String address) {
        byte[] xx = EncodeUtil.toBytes(pubkey);
        Assert.assertEquals(address, CASH.pubKeyHashToLegacy(CipherUtils.pubKeyHash160(xx)));
    }
}
