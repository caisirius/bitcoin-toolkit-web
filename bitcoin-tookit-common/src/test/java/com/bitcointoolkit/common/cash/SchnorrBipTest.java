package com.bitcointoolkit.common.cash;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;

/**
 * Schnorr Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>七月 23, 2019</pre>
 */
public class SchnorrBipTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: schnorr_sign(byte[] msg, BigInteger seckey)
     */
    @Test
    public void testSchnorr_sign() throws Exception {
        testSchnorrSign("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF",
                "243F6A8885A308D313198A2E03707344A4093822299F31D0082EFA98EC4E6C89",
                "7ACBB94884360A26EE5372D16A233DC57CFC3496AD22CAC01F706DDD1CBC0D326B6141694B25612D654B25ECF85E63E61B441269A6544DCE533326B81C3390FE");

        testSchnorrSign("69333436040d1804793a232071010a74793cb3f0fe360ac9c6dbb1465d8631c8",
                "a94636b474de71b150c1bae73e2559f051d473c7df54cbf251c5dc99e443b726",
                "37AA5A0F878519FAAD7C69A01045640F2EA6AC0B5946EABDEEBCAB2B76BDAC6BA624777840FA29D60B4BAE125BBEA0F96DC43B3EF22B75C811DFA5DDB2EE7908");

        testSchnorrSign("12B004FFF7F4B69EF8650E767F18F11EDE158148B425660723B9F9A66E61F747",
                "5255683DA567900BFD3E786ED8836A4E7763C221BF1AC20ECE2A5171B9199E8A",
                "D3B3F6EC2C2E97D0E0DADBD4814ACA608D3D80F5218C3FD1E8A09BD40ECD36F18FE970893E8EC5E6B5A719EF4CD46965F6BF09079BE07729C196E09BD47E06B9");

        testSchnorrSign("180cb41c7c600be951b5d3d0a7334acc7506173875834f7a6c4c786a28fcbb19",
                "02dff1d77f2a671c5f36183726db2341be58feae1da2deced843240f7b502ba6",
                "08112D8B3F66A854C45679C17D0FB3B6C53AD8BF20515EA2A443EFFC015BB350261BED9BD23040565F46CADECE74E480C65E0E0CFA7292D44257B4E298F90CED");


    }

    private void testSchnorrSign(String secKey, String msgStr, String sigStr) {

        BigInteger seckeyNum = new BigInteger(secKey, 16);

        byte[] msg = EncodeUtil.reverseBytes(SchnorrBip.hexStringToByteArray(msgStr));
        String sig_actual = SchnorrBip.bytesToHex(SchnorrBip.schnorr_sign(msg, seckeyNum));

        Assert.assertEquals(sigStr, sig_actual);
    }

    private void testSchnorrSign2(String secKey, String msgStr, String sigStr) {

        byte[] sec = DatatypeConverter.parseHexBinary(secKey);
        byte[] data = EncodeUtil.reverseBytes(DatatypeConverter.parseHexBinary(msgStr));
        BigInteger seckeyNum = new BigInteger(1, sec);

        String sig_actual = SchnorrBip.bytesToHex(SchnorrBip.schnorr_sign(data, seckeyNum));

        Assert.assertEquals(sigStr, sig_actual);
    }

    /**
     * Method: schnorr_verify(byte[] msg, byte[] pubkey, byte[] sig)
     */
    @Test
    public void testSchnorr_verify() throws Exception {
        boolean out = SchnorrBip.schnorr_verify(
                SchnorrBip.hexStringToByteArray("0000000000000000000000000000000000000000000000000000000000000000"),
                SchnorrBip.hexStringToByteArray("0279be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798"),
                SchnorrBip.hexStringToByteArray("31ab79a6591ae4af37c4223bfa75b7396ac9c14950ea78d94afc14be3a6b61dc2a901f43c609dfb10dfe3f00015957f6461d48b61df9f452143ac6f6f7dd4edd")
        );

        Assert.assertTrue(out);

        out = SchnorrBip.schnorr_verify(
                SchnorrBip.hexStringToByteArray("0000000000000000000000000000000000000000000000000000000000000000"),
                SchnorrBip.hexStringToByteArray("0279be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798"),
                SchnorrBip.hexStringToByteArray("787A848E71043D280C50470E8E1532B2DD5D20EE912A45DBDD2BD1DFBF187EF67031A98831859DC34DFFEEDDA86831842CCD0079E1F92AF177F7F22CC1DCED05")
        );
        Assert.assertTrue(out);

        ///

        // 这个 sig 是 bch 计算出来的
        out = SchnorrBip.schnorr_verify(
                EncodeUtil.reverseBytes(
                        SchnorrBip.hexStringToByteArray("02dff1d77f2a671c5f36183726db2341be58feae1da2deced843240f7b502ba6")),
                SchnorrBip.hexStringToByteArray("0301de173aa944eacf7e44e5073baca93fb34fe4b7897a1c82c92dfdc8a1f75ef5"),
                SchnorrBip.hexStringToByteArray("416160c195b0105a22f7fdeab0ecf8dc53f3b7f0292ea0728b98937097e5e96ceaff48e4096353e66b5d7041f36fe9cdd8b81ac8cb6d6970cceaaeaf8505004f")
        );
        Assert.assertTrue(out);

        // 这个 sig 是 bip 的算法计算出来的 都能在 bip的算法下 验证通过
        out = SchnorrBip.schnorr_verify(
                EncodeUtil.reverseBytes(
                        SchnorrBip.hexStringToByteArray("02dff1d77f2a671c5f36183726db2341be58feae1da2deced843240f7b502ba6")),
                SchnorrBip.hexStringToByteArray("0301de173aa944eacf7e44e5073baca93fb34fe4b7897a1c82c92dfdc8a1f75ef5"),
                SchnorrBip.hexStringToByteArray("08112D8B3F66A854C45679C17D0FB3B6C53AD8BF20515EA2A443EFFC015BB350261BED9BD23040565F46CADECE74E480C65E0E0CFA7292D44257B4E298F90CED")
        );
        Assert.assertTrue(out);

        // 这个 sig 是 bch 计算出来的
        out = SchnorrBip.schnorr_verify(
                EncodeUtil.reverseBytes(
                        SchnorrBip.hexStringToByteArray("243F6A8885A308D313198A2E03707344A4093822299F31D0082EFA98EC4E6C89")),
                SchnorrBip.hexStringToByteArray("02dff1d77f2a671c5f36183726db2341be58feae1da2deced843240f7b502ba659"),
                SchnorrBip.hexStringToByteArray("4ca79882190600d3aaaa3e3bcb1ce8bfaac73dbb1a65d7ebc4d3043c6225c9b53e5985f9d0913776d07a7a2b8c0a1e5ed678c4e134a740d618f9d7abbf0cdde9")
        );
        Assert.assertTrue(out);
        // 这个 sig 是 bch 计算出来的
        out = SchnorrBip.schnorr_verify(
                EncodeUtil.reverseBytes(
                        SchnorrBip.hexStringToByteArray("a94636b474de71b150c1bae73e2559f051d473c7df54cbf251c5dc99e443b726")),
                SchnorrBip.hexStringToByteArray("02e03daddbe9da042412d1da4175a033666d10fc289dece62a4b58f47c47f62d2f"),
                SchnorrBip.hexStringToByteArray("c0a1293203748be56499d3de2372c870ad6287454201f959da438fcb59bf4bf32b5888b2e266da9fc6309c2c73e83a9ca706fedc066294a145628fc811803075")
        );
        Assert.assertTrue(out);
    }

} 
