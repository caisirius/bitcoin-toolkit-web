package com.bitcointoolkit.common.cash; 

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/** 
* Base58 Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 2, 2018</pre> 
* @version 1.0 
*/ 
public class Base58Test { 

    @Before
    public void before() throws Exception { 
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: encode(byte[] input) 
    */ 
    @Test
    public void testEncodeCheck() throws Exception {
        String tmp ="1:1233453";
        int idx = tmp.indexOf(':');
        System.out.println(tmp.substring(idx));
        System.out.println(256 % 58);
        String sss = "0100";
        byte[] bytes = EncodeUtil.toBytes(sss);
        System.out.println(Base58.encode(bytes));
        // 测试一些byte头部 得到 第一个字符
        // 0000000000000000000000000000000000000000
        String data = "0000000000000000000000000000000000000000"; // 20个字节
        System.out.println("最小");
        String out = encodeCheck("00", data);
        System.out.println(out);
        out = encodeCheck("05", data);
        System.out.println(out);
        out = encodeCheck("6f", data);
        System.out.println(out);

        data = "ffffffffffffffffffffffffffffffffffffffff"; // 20个字节
        System.out.println("最大");
        out = encodeCheck("00", data); // 实际上不是最大哦 1x6YnuBVeeE65dQRZztRWgUPwyBjHCA5g
        System.out.println(out);
        out = encodeCheck("05", data);
        System.out.println(out);
        out = encodeCheck("6f", data);
        System.out.println(out);
        // ============
        data = "0000000000000000000000000000000000000000";
        out = encodeCheck("68fcfc", data);
        System.out.println("最小："+out);
        data = "ffffffffffffffffffffffffffffffffffffffff";
        out = encodeCheck("68fcfc", data);
        System.out.println("最大："+out);

        // 计算
        for (int i=0;i<255;i++) {
            data = "0000000000000000000000000000000000000000";
            String fcVer = Integer.toHexString(i);
            out = encodeCheck(fcVer, data);
            if (out.startsWith("F")){
                data = "ffffffffffffffffffffffffffffffffffffffff";
                out = encodeCheck(fcVer, data);

                if (out.startsWith("F")){
                    System.out.println("found : " + i + ",hex " + fcVer);
                }
            }
        }

        data = "0000000000000000000000000000000000000000";
        out = encodeCheck("23", data);
        System.out.println("最小："+out);
        data = "ffffffffffffffffffffffffffffffffffffffff";
        out = encodeCheck("23", data);
        System.out.println("最大："+out);

        data = "0000000000000000000000000000000000000000";
        out = encodeCheck("24", data);
        System.out.println("最小："+out);
        data = "ffffffffffffffffffffffffffffffffffffffff";
        out = encodeCheck("24", data);
        System.out.println("最大："+out);

    }

    Random random = new Random(System.currentTimeMillis());

    /**
     * 计算地址以 F 开头的概率
     */
    private double countRatio(int version) {
        int total = 5000000;
        int success = 0;
        String hexStr = Integer.toHexString(version);
        if (hexStr.length() == 1) {
            hexStr = "0" + hexStr;
        }
        for (int i=0; i< total; i++) {
            String data = randomByteStr(20);

            String out = encodeCheck(hexStr, data);
            if (out.startsWith("F")){
                success++;
            }
        }
        return success * 1.0 / total;
    }

    private String randomByteStr(int bytes) {
        StringBuffer data = new StringBuffer();
        for (int i=0;i<bytes;i++){// 20 bytes
            String str = Integer.toHexString(random.nextInt(256));
            if (str.length() == 1) {
                str = "0" + str;
            }
            data.append(str);
            if (str.length()!=2) {
                System.err.println(str);
            }
        }
        return data.toString();
    }

    private String encodeCheck(String prefix, String data) {
        byte[] bytes = EncodeUtil.toBytes(prefix + data);
        return Base58.encodeCheck(bytes);
    }

    /** Method: decode(String input)
    */ 
    @Test
    public void testDecode() throws Exception { 
    //TODO: Test goes here... 
    } 

    /** Method: decodeToBigInteger(String input) 
    */ 
    @Test
    public void testDecodeToBigInteger() throws Exception {
        String str = "ffff";
        BigInteger bigint = Base58.decodeToBigInteger(str);
    } 

    /** Method: decodeChecked(String input) 
    */ 
    @Test
    public void testDecodeChecked() throws Exception {
        // decodeChecked
        String out ="33vt8ViH5jsr115AGkW6cEmEz9MpvJSwDk";
        byte[] decodedBytes = Base58.decodeChecked(out);
        String decodedStr = EncodeUtil.parseToString(decodedBytes);

        String data = "123456789abcdef0";
        testEncodeAndDecodeCheck("00", data);
        testEncodeAndDecodeCheck("05", data);

        testEncodeAndDecodeCheck("00", "0000000000000000000000000000000000000000");
        testEncodeAndDecodeCheck("00", EncodeUtil.parseToString(CipherUtils.pubKeyHash160(data.getBytes())));
    }

    private void testEncodeAndDecodeCheck(String prefix, String data) throws NoSuchAlgorithmException {
        String str = prefix + data;// 编码前
        byte[] bytes = EncodeUtil.toBytes(str);
        String out = Base58.encodeCheck(bytes);

        byte[] decodedBytes = Base58.decodeChecked(out);
        String decodedStr = EncodeUtil.parseToString(decodedBytes);
        Assert.assertEquals(str, decodedStr);
    }

    /** 
    * 不同version 头的地址转换测试
    */ 
    @Test
    public void testConvert() throws Exception {

        String newAddr = convertPrefix("1MBpEjqgEMQpyLD7H4d1q7TGBD96jYEbDc", 2, "6f");

        Assert.assertEquals("n1hmXnvf3Nr5kSgizdbPf2fb3Cjob5DvRu", newAddr);

    }

    private String convertPrefix(String address, int oldPrefixSize, String newPrefix) throws NoSuchAlgorithmException {
        byte[] decodedBytes = Base58.decodeChecked(address);

        String decodedStr = EncodeUtil.parseToString(decodedBytes);
        String str = newPrefix + decodedStr.substring(oldPrefixSize);

        return Base58.encodeCheck(EncodeUtil.toBytes(str));
    }



} 
