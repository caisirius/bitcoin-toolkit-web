package com.bitcointoolkit.common.cash; 

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* StrEncodingUtil Tester. 
* 
* @author <Authors name> 
* @since <pre>六月 17, 2018</pre> 
* @version 1.0 
*/ 
public class EncodeUtilTest {

    @Before
    public void before() throws Exception { 
    } 

    @After
    public void after() throws Exception { 
    } 

    @Test
    public void print() {
        String str = "020000000001016bdf6a5fef2b019d5acf4c29a6730f5011c86d4eec340fdf79584506de97623b0100000017160014e8f6002e2336e40c22dcdb5b80c7eeef2b5d405fffffffff01c0878b3b0000000017a9141a283442bd9961bfa3e572bea276505f14b8f7158702473044022069c41acf0c377763ca48bcac20bcb0956a9320b9e5a404a5847816ba1a13dd69022023a75d6b5da3cb82efa050d2f64ea4b46ba83b7dde66cf6aebf1845a7f8173d20121029336bea825e6b65a732bc1e2588ab4b7364b74a42ce2318ebea3bba44aa200d100000000";
        int[] bbb = EncodeUtil.toInts(str);

        Assert.assertEquals(str, EncodeUtil.parseToString(bbb));
    }

    @Test
    public void parseInt16Test() {
        int[] in = new int[]{1,0};
        Assert.assertEquals(1, EncodeUtil.parseInt16(in));

        in = new int[]{0,222};
        Assert.assertEquals(56832, EncodeUtil.parseInt16(in));
    }


    @Test
    public void parseInt32Test() {
        int[] in = new int[]{1,0,0,0};
        Assert.assertEquals(1, EncodeUtil.parseInt32(in));

        in = new int[]{0,148,53,119};
        Assert.assertEquals(2000000000, EncodeUtil.parseInt32(in));

        in = new int[]{0,0,0,218};// 大于 Int.Max
        Assert.assertEquals(3657433088L, EncodeUtil.parseInt32(in));
    }

    @Test
    public void parseInt64Test() {
        int[] in = new int[]{1,0,0,0,0,0,0,0};
        Assert.assertEquals(1, EncodeUtil.parseInt64(in));

        in = new int[]{0,148,53,119,0,0,0,0};
        Assert.assertEquals(2000000000, EncodeUtil.parseInt64(in));

        in = new int[]{0,0,0,255,0,0,1,0};// 大于 Int.Max
        Assert.assertEquals(281479254900736L, EncodeUtil.parseInt64(in));
    }

    @Test
    public void parseBigEndianToStringTest(){
        String str = "0113880d01bb078eeabfc686d4b23e43607520c3c2e31b550eb5749faf18c1b9";
        String expect = "b9c118af9f74b50e551be3c2c3207560433eb2d486c6bfea8e07bb010d881301";

        int[] bbb = EncodeUtil.toInts(str);
        Assert.assertEquals(expect, EncodeUtil.parseBigEndianToString(bbb));

        byte[] bbs = new byte[]{1,2,3};
        Assert.assertEquals("030201", EncodeUtil.parseBigEndianToString(bbs));

    }

    @Test
    public void testSubInts() {
        int[] ints = new int[]{1,2,3,4,5,6,7,8,9,0};

        int[] subs = EncodeUtil.subInt(ints, 1, 3);
        Assert.assertEquals(2, subs.length);
        Assert.assertEquals(2, subs[0]);
    }

} 
