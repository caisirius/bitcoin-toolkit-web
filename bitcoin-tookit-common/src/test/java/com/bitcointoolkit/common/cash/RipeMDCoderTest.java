package com.bitcointoolkit.common.cash; 

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;

import java.security.Security;

/** 
* RipeMDCoder Tester. 
* 
* @author <Authors name> 
* @since <pre>六月 19, 2018</pre> 
* @version 1.0 
*/ 
public class RipeMDCoderTest { 

    @Before
    public void before() throws Exception { 
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: encodeRipeMD128(byte[] data) 
    */ 
    @Test
    public void test() throws Exception {
        String str="RIPEMD消息摘要";
        System.out.println("原文："+str);
        byte[] data1=RipeMDCoder.encodeRipeMD128(str.getBytes());
        String data1hex=RipeMDCoder.encodeRipeMD128Hex(str.getBytes());
        System.out.println("RipeMD128的消息摘要算法值："+data1.toString());
        System.out.println("RipeMD128的十六进制消息摘要算法值："+data1hex);
        System.out.println();

        byte[] data2=RipeMDCoder.encodeRipeMD160(str.getBytes());
        String data2hex=RipeMDCoder.encodeRipeMD160Hex(str.getBytes());
        System.out.println("RipeMD160的消息摘要算法值："+data2.toString());
        System.out.println("RipeMD160的十六进制消息摘要算法值："+data2hex);
        System.out.println();

        byte[] data3=RipeMDCoder.encodeRipeMD256(str.getBytes());
        String data3hex=RipeMDCoder.encodeRipeMD256Hex(str.getBytes());
        System.out.println("RipeMD256的消息摘要算法值："+data3.toString());
        System.out.println("RipeMD256的十六进制消息摘要算法值："+data3hex);
        System.out.println();

        byte[] data4=RipeMDCoder.encodeRipeMD320(str.getBytes());
        String data4hex=RipeMDCoder.encodeRipeMD320Hex(str.getBytes());
        System.out.println("RipeMD320的消息摘要算法值："+data4.toString());
        System.out.println("RipeMD320的十六进制消息摘要算法值："+data4hex);
        System.out.println();

        System.out.println("================以下的算法支持是HmacRipeMD系列，现阶段只有BouncyCastle支持=======================");
        //初始化密钥
        byte[] key5=RipeMDCoder.initHmacRipeMD128Key();
        //获取摘要信息
        byte[] data5=RipeMDCoder.encodeHmacRipeMD128(str.getBytes(), key5);
        String datahex5=RipeMDCoder.encodeHmacRipeMD128Hex(str.getBytes(), key5);
        System.out.println("Bouncycastle HmacRipeMD128的密钥:"+key5.toString());
        System.out.println("Bouncycastle HmacRipeMD128算法摘要："+data5.toString());
        System.out.println("Bouncycastle HmacRipeMD128Hex算法摘要："+datahex5.toString());
        System.out.println();

        //初始化密钥
        byte[] key6=RipeMDCoder.initHmacRipeMD160Key();
        //获取摘要信息
        byte[] data6=RipeMDCoder.encodeHmacRipeMD160(str.getBytes(), key6);
        String datahex6=RipeMDCoder.encodeHmacRipeMD160Hex(str.getBytes(), key6);
        System.out.println("Bouncycastle HmacRipeMD160的密钥:"+key6.toString());
        System.out.println("Bouncycastle HmacRipeMD160算法摘要："+data6.toString());
        System.out.println("Bouncycastle HmacRipeMD160Hex算法摘要："+datahex6.toString());
        System.out.println();
    }


} 
