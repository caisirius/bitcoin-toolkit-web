package com.bitcointoolkit.common.util;

import org.junit.Test;

import java.io.IOException;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public class HttpsUtilsTest {

    @Test
    public void testHttpsCall() {
        HttpsUtil util = new HttpsUtil();
        long start = System.currentTimeMillis();
        String uri = "https://baidu.com";
        try {
            byte[] bytes = HttpsUtil.doGet(uri);
//        System.out.println(new String(bytes));
        } catch (IOException e) {
            e.printStackTrace();
        }

        long cost = System.currentTimeMillis() - start;
        System.out.println("cost:" + cost);

        start = System.currentTimeMillis();
        uri = "https://baidu.com";
        try {
            byte[] bytes = HttpsUtil.doPost(uri, "a");
        } catch (IOException e) {
            e.printStackTrace();
        }

        cost = System.currentTimeMillis() - start;
        System.out.println("cost:" + cost);
    }
}
