package com.bitcointoolkit.web.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * create by tanweia on Jun 8, 2017
 */
public class DateUtil {

    public static final String DATETIME_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String SHOW_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SHOW_DATETIME_FORMATS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String SHOW_DATE_FORMAT = "yyyy-MM-dd";
    public static Date TIME_2099 = null;

    private static final long DAY = (long) 24 * 3600 * 1000;

    /**
     * @param date      时间
     * @param formatStr 格式化串
     * @return
     */
    public static String format(Date date, String formatStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        return sdf.format(date);
    }

    public static String format(Date date) {
        return format(date, SHOW_DATETIME_FORMAT);
    }


    /**
     * 时间转换成日期
     */
    public static Date parseDate(String parseTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(parseTime);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        return date;
    }

    public static Date parseDate2(String parseTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(parseTime);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        return date;
    }


    /**
     * Title: preDay
     * Description:  获取后n天
     *
     * @param start
     * @return
     */
    public static Date afterDay(Date start, int n) {
        Calendar c = Calendar.getInstance();
        c.setTime(start);
        c.add(Calendar.DATE, n);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * 获取当前周
     *
     * @param date
     * @return
     */
    public static int getWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String monthOfYear = cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : "" + (cal.get(Calendar.MONTH) + 1);
        int year = cal.get(Calendar.YEAR);
        String weekOfYear = cal.get(Calendar.WEEK_OF_YEAR) < 10 ? "0" + cal.get(Calendar.WEEK_OF_YEAR) : "" + cal.get(Calendar.WEEK_OF_YEAR);
        String week = year + weekOfYear;
        return Integer.valueOf(week);
    }

    /**
     * 获取当前月
     *
     * @param date
     * @return
     */
    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String monthOfYear = cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : "" + (cal.get(Calendar.MONTH) + 1);
        int year = cal.get(Calendar.YEAR);
        String month = year + monthOfYear;
        return Integer.valueOf(month);
    }

    /**
     * 获取当前时间零点
     */
    public static Date currentDay(Date start) throws ParseException {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        return dft.parse(dft.format(start));

    }

    /**
     * 获取任意一天
     *
     * @return
     */
    public static Date anyDay(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, day);
        return c.getTime();
    }

    /**
     * 获取过去一年
     *
     * @return
     */
    public static Date getLastYear(Date date) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, -1);
        Date y = c.getTime();
        return y;
    }

    /**
     * 指定任意时间是一年中第几周
     */
    public static int getWeekofYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int week = cal.get(Calendar.WEEK_OF_YEAR);
        return week;
    }

    /**
     * 指定时间是第几年
     */
    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        return year;
    }

    /**
     * 将时间转为整数
     */
    public static String parse(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String time = format.format(date);
        String substring = time.replaceAll("-", "").substring(0, 8);
        return substring;
    }
}
