package com.bitcointoolkit.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 * Biz模块的java config配置
 * @author caisirius
 */
@Configuration
@ComponentScan(basePackages = { "com.bitcointoolkit" }, includeFilters = { @ComponentScan.Filter(value = Service.class) })
public class BizConfig {

}
