package com.bitcointoolkit.web.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.bitcointoolkit.web.interceptor.PretreatmentInterceptor;
import com.bitcointoolkit.web.interceptor.SecurityInterceptor;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * 如果加上了@EnableWebMvc 则需要完全自己控制mvc的配置
 * 否则spring使用WebMvcAutoConfiguration自动配置
 * 自己实现一个类型为WebMvcConfigurerAdapter的bean可更改你需要更改的配置，其他配置仍照用spring boot默认
 * /static (or /public or /resources or /META-INF/resources)这些路径会默认配置成静态资源
 * @author caisirius
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
	public void addInterceptors(InterceptorRegistry registry) {
    	
        //单点登录拦截
//        registry.addInterceptor(ssoInterceptor()).addPathPatterns("/**");
        
        //安全拦截
        registry.addInterceptor(securityInterceptor()).addPathPatterns("/**");
        
        //角色权限拦截
//        registry.addInterceptor(roleCheckInterceptor()).addPathPatterns("/**");
        
        //模块权限拦截（业务逻辑性质）
//        registry.addInterceptor(permissionCheckInterceptor()).addPathPatterns("/*/save","/*/delete","/*/copy","/*/edit","/*/recover");
        
        //预处理拦截
    	registry.addInterceptor(pretreatmentInterceptor()).addPathPatterns("/**");

    }


	@Bean  
    public FilterRegistrationBean xssFilterRegistrationBean(){  
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();  
        filterRegistrationBean.setFilter(new XSSFilter());  
        filterRegistrationBean.setName("xssFilter");
        filterRegistrationBean.setEnabled(true);  
        filterRegistrationBean.addUrlPatterns("/*");  
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/static/*,/resources/*");
        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;  
    }     
    
    @Bean  
    public FilterRegistrationBean druidFilterRegistrationBean(){  
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();  
        filterRegistrationBean.setFilter(new WebStatFilter());  
        filterRegistrationBean.setName("druidStatFilter");
        filterRegistrationBean.setEnabled(true);  
        filterRegistrationBean.addUrlPatterns("/*");  
        //忽略资源
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.setOrder(2);
        return filterRegistrationBean;  
    }  
    
    @Bean  
    public ServletRegistrationBean druidViewSevletRegistrationBean(){  
    	ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(); 
    	servletRegistrationBean.setServlet(new StatViewServlet());
    	servletRegistrationBean.setName("druidStatViewSevlet");
    	servletRegistrationBean.addUrlMappings("/druid/*");
    	servletRegistrationBean.addInitParameter("allow", "");// IP白名单 (没有配置或者为空，则允许所有访问)
    	servletRegistrationBean.addInitParameter("loginUsername", "zbj");// 用户名
    	servletRegistrationBean.addInitParameter("loginPassword", "123456");// 密码
    	servletRegistrationBean.addInitParameter("resetEnable", "false");// 禁用HTML页面上的“Reset All”功能
        return servletRegistrationBean;  
    }

    @Bean
    public SecurityInterceptor securityInterceptor(){
    	SecurityInterceptor interceptor = new SecurityInterceptor();
        interceptor.addNotFilter("/error");
        interceptor.addNotFilter("/resources/**");
        interceptor.addNotFilter("/static/**");
        interceptor.addNotFilter("/boss/**");
        interceptor.addNotFilter("/logout");
        interceptor.addNotFilter("/forbidden");
    	return interceptor;
    }
    
    @Bean
    public PretreatmentInterceptor pretreatmentInterceptor(){
    	PretreatmentInterceptor interceptor = new PretreatmentInterceptor();
        interceptor.addNotFilter("/error");
        interceptor.addNotFilter("/boss/**");
        interceptor.addNotFilter("/logout");
        interceptor.addNotFilter("/forbidden");
    	return interceptor;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

//    @Bean
//    public JetTemplateViewResolver viewResolver() {
//        JetTemplateViewResolver jetTemplateViewResolver = new JetTemplateViewResolver();
//        jetTemplateViewResolver.setOrder(1);
//        jetTemplateViewResolver.setContentType("text/html; charset=utf-8");
//        jetTemplateViewResolver.setSuffix(".html");
//        jetTemplateViewResolver.setConfigLocation("classpath:/jetbrick-template.properties");
//        Properties configProperties = new Properties();
//        configProperties.setProperty("jetx.input.encoding", "UTF-8");
//        configProperties.setProperty("jetx.output.encoding", "UTF-8");
//        jetTemplateViewResolver.setConfigProperties(configProperties);
//        return jetTemplateViewResolver;
//    }

}
