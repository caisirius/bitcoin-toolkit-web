package com.bitcointoolkit.web.service;

import com.alibaba.fastjson.JSON;
import com.bitcointoolkit.common.util.HttpsUtil;
import com.bitcointoolkit.web.support.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * 从 blockdozer.com 获取信息
 *
 * @author caican
 * @date 18/5/18
 */
@Service(value = "addrExecutorBlockdozer")
public class AddrExecutorBlockdozer implements IAddrExecutor {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public Result<UtxoResult> getUtxoOfLegacy(String legacy, int pageNum, int pageSize) {
        Result<UtxoResult> result = Result.create();
        String uri = "https://blockdozer.com/api/addr/"+legacy+"/utxo";
        try {
            byte[] bytes = HttpsUtil.doGet(uri);
            String ret = new String(bytes);
            List<Utxo> utxos = JSON.parseArray(ret,Utxo.class);
            UtxoResult data = new UtxoResult();
            data.setUtxos(utxos);
            data.setPage(1);
            data.setPageSize(-1);
            data.setTotalCount(utxos.size());
            return result.success(data);
        } catch (IOException e) {
            logger.error("", e);
            return result.fail("","failed");
        }
    }
}
