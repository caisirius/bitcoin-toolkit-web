package com.bitcointoolkit.web.service.tx;

/**
   交易类型	    描述
 TX_NONSTANDARD	非标准的交易
 TX_PUBKEY	    公钥
 TX_PUBKEYHASH	公钥哈希
 TX_SCRIPTHASH	脚本哈希
 TX_MULTISIG	多重签名
 TX_NULL_DATA	空数据
 * @author caican
 * @date 18/6/17
 */
public enum TxType {

    /**
     * 无效
     */
    TX_NONSTANDARD,
    // 'standard' transaction types:
    /**
     * 公钥
     */
    TX_PUBKEY,
    /**
     * 公钥哈希
     */
    TX_PUBKEYHASH,
    /**
     * 脚本哈希
     */
    TX_SCRIPTHASH,
    /**
     * 多重签名
     */
    TX_MULTISIG,
    /**
     * 空数据
     */
    TX_NULL_DATA
}
