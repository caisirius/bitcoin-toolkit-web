package com.bitcointoolkit.web.service.tx;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/6/17
 */
public class TxOut {

    @JSONField(serialize=false)
    private TxScript scriptPubKey;// 输出脚本

    public void setTxScript(String scriptHex) {
        scriptPubKey = new TxScriptPubkey(scriptHex);
        this.scriptHex = scriptHex;
        this.scriptAsm = scriptPubKey.getScriptAsm();
        this.addresses = ((TxScriptPubkey)scriptPubKey).getAddresses();
        this.type = ((TxScriptPubkey) scriptPubKey).getType();
    }

    /**
     * 计算序列化后的字节数
     * amount:8 + len:1 + 脚本字节数
     */
    public int countSize(){
        return 8 + 1 + scriptHex.length()/2;
    }

    // 以下是 JSON 反序列化字段
    private long value;//输出金额
    private int n;// 序号
    private List<String> addresses;// 输出地址
    private String scriptHex;
    private String scriptAsm;
    private String type;

    public long getValue() {
        return value;
    }

    public int getN() {
        return n;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public String getScriptHex() {
        return scriptHex;
    }

    public String getScriptAsm() {
        return scriptAsm;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public void setN(int n) {
        this.n = n;
    }

    public String getType() {
        return type;
    }
}
