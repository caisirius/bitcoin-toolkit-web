package com.bitcointoolkit.web.service;

import com.bitcointoolkit.web.support.Result;


/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public interface IAddrExecutor {

    /** 获取地址的UTXO，目前是从外部api获取
     * @param legacy 地址，base58格式
     * @param pageNum 页数
     * @param pageSize 每页条数
     * @return ret
     */
    Result<UtxoResult> getUtxoOfLegacy(String legacy, int pageNum, int pageSize);
}
