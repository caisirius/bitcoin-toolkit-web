package com.bitcointoolkit.web.service;

import com.alibaba.fastjson.JSON;
import com.bitcointoolkit.common.util.HttpsUtil;
import com.bitcointoolkit.web.support.Result;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 从 btc.com 获取信息
 *
 * @author caican
 * @date 18/5/18
 */
@Service(value = "addrExecutorBtccom")
public class AddrExecutorBtccom implements IAddrExecutor {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Result<UtxoResult> getUtxoOfLegacy(String legacy, int pageNum, int pageSize) {
        Result<UtxoResult> result = Result.create();
        if (pageNum < 1 || pageSize < 1){
            throw new IllegalArgumentException("pageNum or pageSize illegal");
        }
        if (pageSize > 50) {
            pageSize = 50;
        }
        String uri = "https://bch-chain.api.btc.com/v3/address/"+legacy+"/unspent?page="+pageNum+"&pagesize="+pageSize;
        try {
            byte[] bytes = HttpsUtil.doGet(uri);
            String ret = new String(bytes);

            return transfer(ret);
        } catch (IOException e) {
            logger.error("", e);
            return result.fail("","failed");
        }
    }

    public Result<UtxoResult> transfer(String ret) {
        UtxoResultBchcom in = JSON.parseObject(ret, UtxoResultBchcom.class);

        Result<UtxoResult> result = Result.create();
        if (in.errNo != 0) {
            return result.fail("", "获取数据失败：" +in.errMsg);
        }
        UtxoResult utxoResult = new UtxoResult();
        utxoResult.setTotalCount(in.getData().getTotalCount());
        utxoResult.setPageSize(in.getData().getPagesize());
        utxoResult.setPage(in.getData().getPage());
        List<Utxo> utxos = new ArrayList();
        utxoResult.setUtxos(utxos);

        for (UtxoResultBchcomDetail data : in.data.list) {
            Utxo utxo = new Utxo();
            utxo.setTxid(data.txHash);
            utxo.setVout(data.txOutputN);
            utxo.setConfirmations(data.confirmations);
            utxo.setSatoshis(data.value);
            utxo.setAmount(new BigDecimal(data.value).divide(new BigDecimal(100000000)));
            utxos.add(utxo);
        }
        return result.success(utxoResult);
    }

    @Data
    public static class UtxoResultBchcom {
        private UtxoResultBchcomData data;
        private int errNo;
        private String errMsg;
    }
    @Data
    public static class UtxoResultBchcomData {
        private int totalCount;
        private int page;
        private int pagesize;
        private List<UtxoResultBchcomDetail> list;

    }

    @Data
    public static class UtxoResultBchcomDetail {
        private String txHash;
        private int txOutputN;
        private int txOutputN2;
        private long value;
        private int confirmations;
    }
}
