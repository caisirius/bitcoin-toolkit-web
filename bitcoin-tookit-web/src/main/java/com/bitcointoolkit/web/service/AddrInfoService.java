package com.bitcointoolkit.web.service;

import com.bitcointoolkit.web.support.Result;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public interface AddrInfoService {

    /**查询UTXO
     * @param address 地址 legacy格式
     * @param pageNum 页数
     * @param pageSize 每页条数
     * @return UtxoResult
     */
    Result<UtxoResult> listUtxo(String address, int pageNum, int pageSize);

}
