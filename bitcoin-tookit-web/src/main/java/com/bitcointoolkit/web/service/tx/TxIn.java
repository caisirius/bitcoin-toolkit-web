package com.bitcointoolkit.web.service.tx;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/6/17
 */
public class TxIn {

    @JSONField(serialize=false)
    private TxScript scriptSig;// 输入脚本

    public void setTxScript(String scriptHex) {
        scriptSig = new TxScriptSig(scriptHex);
        this.scriptHex = scriptHex;
        this.scriptAsm = scriptSig.getScriptAsm();

    }

    /**
     * 计算序列化后的字节数
     * txid:32 + n:4 + len:1 + 脚本字节数 + sequence:4
     */
    public int countSize(){
        return 32 + 4 + scriptSig.getLenOfLen() + scriptHex.length()/2 + 4;
    }
    // 以下是 JSON 反序列化字段
    private OutPoint prevout;
    private String scriptHex;
    private String scriptAsm;
    private long sequence;

    public OutPoint getPrevout() {
        return prevout;
    }

    public void setPrevout(OutPoint prevout) {
        this.prevout = prevout;
    }

    public String getScriptHex() {
        return scriptHex;
    }

    public String getScriptAsm() {
        return scriptAsm;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }
}
