package com.bitcointoolkit.web.service;

import java.util.List;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/29
 */
public class UtxoResult {
    private int totalCount;
    private int page;
    private int pageSize;
    private List<Utxo> utxos;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<Utxo> getUtxos() {
        return utxos;
    }

    public void setUtxos(List<Utxo> utxos) {
        this.utxos = utxos;
    }
}
