package com.bitcointoolkit.web.service.tx;

import com.bitcointoolkit.web.support.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * [类描述]
 *
 * @author caican
 * @date 2018/6/26
 */
@Service
public class TxServiceImpl implements TxService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Result<DecodedTx> decodeRawTx(String rawhex) {
        Result<DecodedTx> result = Result.create();
        DecodedTx decodedTx = new DecodedTx();
        try {
            decodedTx.decode(rawhex);
            return result.success(decodedTx);
        }catch (Exception e) {
            logger.error("decodeRawTx error!", e);
            return result.fail("", "decode failed:" + e.getMessage());
        }
    }
}
