package com.bitcointoolkit.web.service.tx;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/6/17
 */
public class OutPoint {
    private String txid;
    private int n;// 序号

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
