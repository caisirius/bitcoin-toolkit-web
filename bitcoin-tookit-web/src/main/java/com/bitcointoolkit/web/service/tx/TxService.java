package com.bitcointoolkit.web.service.tx;

import com.bitcointoolkit.web.service.IAddrExecutor;
import com.bitcointoolkit.web.service.UtxoResult;
import com.bitcointoolkit.web.support.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public interface TxService {

    /** 解码验证交易rawhex
     * @param rawhex
     * @return
     */
    Result<DecodedTx> decodeRawTx(String rawhex);
}
