package com.bitcointoolkit.web.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class BaseInterceptor extends HandlerInterceptorAdapter{
	
	private static final Logger log = LoggerFactory.getLogger(BaseInterceptor.class);
	
	private PathMatcher pathMatcher = new AntPathMatcher();

	private List<String> notFilterPatterns;
	
	// 不过滤的URI
	protected boolean isNotFilter(HttpServletRequest request) {
		if (notFilterPatterns != null) {
			for (String notFilter : notFilterPatterns) {
				if (pathMatcher.match(notFilter, request.getRequestURI())) {
					log.debug("request uri ["+request.getRequestURI()+"] matches the not filter pattern ["+notFilter+"],the interceptor breaks out.");
					return true;
				}
			}
		}
		return false;
	}
	
	// 增加pattern
	public void addNotFilter(String pattern) {
		if (notFilterPatterns == null) {
			notFilterPatterns = new ArrayList<String>();
		}
		notFilterPatterns.add(pattern);
	}
}
