package com.bitcointoolkit.web;

import com.bitcointoolkit.dao.config.MyBatisConfig;
import com.bitcointoolkit.web.config.BizConfig;
import com.bitcointoolkit.web.config.QuartzConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author caisirius
 */
@SpringBootApplication  // same as @Configuration @EnableAutoConfiguration @ComponentScan
@EnableTransactionManagement
@PropertySource(
		ignoreResourceNotFound = true,
		value = { "classpath:application.properties", "classpath:conf.properties", "${conf.properties}" })
@Import(value = {BizConfig.class,MyBatisConfig.class, QuartzConfig.class})
public class Application extends SpringBootServletInitializer{
	
	/**
	 * 支持外部J2EE容器启动
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
		/**
		 //高级配置
		 SpringApplication app = new SpringApplication(Application.class);
		 //一些个性化的设置，让spring产生你需要的applicationContexjt
		 app.setWebEnvironment(true);//显式设定是否启用webMVC，默认会根据classpath是否包含spring-webmvc来启用
		 ApplicationContext ctx = app.run(args);
		 System.out.println("Let's inspect the beans provided by Spring Boot:");
		 String[] beanNames = ctx.getBeanDefinitionNames();
		 Arrays.sort(beanNames);
		 for (String beanName : beanNames) {
			 System.out.println(beanName);
		 }
		**/ 
	}

}
