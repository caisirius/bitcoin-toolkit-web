package com.bitcointoolkit.web.support;

import com.alibaba.fastjson.JSON;

import java.util.Map;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public class ErrorResult {
    private ErrorInfo error;

    public ErrorResult(int code, String msg) {
        error = new ErrorInfo(code, msg);
    }

    public ErrorResult(ErrorEnum err) {
        error = new ErrorInfo(err.getCode(), err.getMsg());
    }


    public ErrorInfo getError() {
        return error;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    public class ErrorInfo {
        private Integer code;
        private String msg;
        public ErrorInfo(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
