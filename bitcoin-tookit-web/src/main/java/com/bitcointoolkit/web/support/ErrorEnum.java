package com.bitcointoolkit.web.support;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public enum ErrorEnum {
    /**
     * 参数非法
     */
    ARGUMENT_INVALID(1, "argument invalid!"),
    /**
     * 地址非法
     */
    ADDRESS_INVALID(10, "address format is invalid!"),
    /**
     * 地址前缀非法
     */
    INVALID_PREFIX(11, "address prefix is invalid!"),
    /**
     * 转换异常
     */
    CONVERT_ERROR(12, "convert error"),
    /**
     * 查询失败
     */
    QUERY_FAILED(13, "query failed");
    private String msg;
    private int code;

    private ErrorEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}
