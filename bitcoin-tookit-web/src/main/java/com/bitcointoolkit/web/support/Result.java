package com.bitcointoolkit.web.support;

/**
 * 结果类,提供简化的dubbo接口返回类<br/>
 * Result&lt;XXX&gt; result = Result.create();<br/>
 * ...<br/>
 * return result.success();<br/>
 * or <br/>
 * return result.success(data);<br/>
 * or <br/>
 * return result.fail("SomeErrorCode","SomeDescription")<br/>
 * or <br/>
 * return result.fail("SomeErrorCode") <br/>
 * or you can do chained callings like below:<br/>
 *
 * result.data(data).code("SomeCode").description("SomeDescription").success();<br/>
 * <br/>
 * @author caisirius
 */
public class Result<T> {

	/**
	 * 请求标识号
	 */
	private String sid;

	/** 成功标志*/
	private boolean success;

	/** 信息码 */
	private String code;

	/** 描述 */
	private String msg;
	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}
	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * 返回数据，可为基本类型（包装类），可以为其它可序列化对象
	 */
	private T data;

	public static <T> Result<T> create() {
		Result<T> result = new Result<T>();
		result.setSuccess(false);
		return result;
	}

	public Result<T> success(){
		success(null);
		return this;
	}

	public Result<T> success(T data){
		this.setSuccess(true);
		this.data = data;
		return this;
	}

	public Result<T> fail(String code,String msg){
		this.setSuccess(false);
		this.setCode(code);
		this.setMsg(msg);
		return this;
	}

	public Result<T> fail(String code){
		fail(code, null);
		return this;
	}

	public Result<T> code(String code){
		this.setCode(code);
		return this;
	}

	public Result<T> msg(String msg){
		this.setMsg(msg);
		return this;
	}

	public Result<T> sid(String sid){
		this.setSid(sid);
		return this;
	}

	public Result<T> data(T data){
		this.data = data;
		return this;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
