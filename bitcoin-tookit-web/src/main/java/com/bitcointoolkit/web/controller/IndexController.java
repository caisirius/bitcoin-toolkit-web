package com.bitcointoolkit.web.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController extends BaseController {

	/**
	 * 首页
	 */
	@RequestMapping({"/index",""})
	public String index(Model model) {
		return "index";
	}


	@RequestMapping({"/json"})
	@ResponseBody
	public String json(Model model) {
		Map<String,Object> map = new HashMap<>();
		map.put("name","cc");
		map.put("age",12);
		return JSON.toJSONString(map);
	}
	
}
