package com.bitcointoolkit.web.controller;

/**
 * [类描述]
 *
 * @author caican
 * @date 18/5/18
 */
public class AddressInfo {
    private String cashaddr;
    private String legacy;
    private String copay;
    private String hash;

    public String getCashaddr() {
        return cashaddr;
    }

    public void setCashaddr(String cashaddr) {
        this.cashaddr = cashaddr;
    }

    public String getLegacy() {
        return legacy;
    }

    public void setLegacy(String legacy) {
        this.legacy = legacy;
    }

    public String getCopay() {
        return copay;
    }

    public void setCopay(String copay) {
        this.copay = copay;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
