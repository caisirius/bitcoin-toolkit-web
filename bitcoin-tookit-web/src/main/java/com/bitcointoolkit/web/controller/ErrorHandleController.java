package com.bitcointoolkit.web.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Spring Boot 对MVC的error处理机制
 * 暂时注释掉
 * 只需要实现ErrorController,然后映射/error即可
 * @author caisirius
 */
//@Controller
public class ErrorHandleController implements ErrorController {  
	
	@Override
    public String getErrorPath() {
        return "error/error";
    }
//
//    @RequestMapping("/error")
//    public String errorHandle(){
//        return getErrorPath();
//    }
//
}  
