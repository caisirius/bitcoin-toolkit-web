package com.bitcointoolkit.web.service.tx; 

import com.alibaba.fastjson.JSON;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* DecodedTx Tester. 
* 
* @author <Authors name> 
* @since <pre>六月 17, 2018</pre> 
* @version 1.0 
*/ 
public class DecodedTxTest {

    DecodedTx decodedTx;
    @Before
    public void before() throws Exception {
        decodedTx = new DecodedTx();
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: decode(String rawhex) 
    */ 
    @Test
    public void testDecode() throws Exception {
        String input = "0100000001ff13880d01bb078eeabfc686d4b23e43607520c3c2e31b550eb5749faf18c1b9000000006a47304402202c5e5424e83e44893eb172f0bdd193a2fd825787ad80d5515065ddbf6dd887a4022071335f4fd2736dce7a199b2e56ecf972e086e5d9410691dfb6bdfd3d22a16f0e4121026173240c95bd7e70c870b74419ee58c30f3edf47b3696ab8865c688291a89a9dffffffff0200943577000000001976a914dd6e1b4d5799b0c17ee89ac3724563b44f7f47f588acf036d0b2000000001976a9144a54e6d25321cc5447eabd61d54373022342ffc388ac00000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
        Assert.assertEquals(2, decodedTx.getVout().size());
        Assert.assertEquals(1, decodedTx.getVin().size());
        Assert.assertEquals(TxType.TX_PUBKEYHASH.toString(), decodedTx.getVout().get(0).getType());
        Assert.assertEquals("1MBpEjqgEMQpyLD7H4d1q7TGBD96jYEbDc",
                decodedTx.getVout().get(0).getAddresses().get(0));

        input = "010000000156211389e5410a9fd1fc684ea3a852b8cee07fd15398689d99441b98bfa76e290000000000ffffffff0280969800000000001976a914fdc7990956642433ea75cabdcc0a9447c5d2b4ee88acd0e89600000000001976a914d6c492056f3f99692b56967a42b8ad44ce76b67a88ac00000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
        Assert.assertEquals(2, decodedTx.getVout().size());
        Assert.assertEquals(1, decodedTx.getVin().size());
        Assert.assertEquals(TxType.TX_PUBKEYHASH.toString(), decodedTx.getVout().get(0).getType());


        // multisig scriptPubKey
        input = "010000000176416613351a01b9a7b351339c608ae93829ea7723afed9e501467de77f313bf030000006b48304502203cb2b3dfeddaa2790d5ee4bf91fe42c429c583e1189905cfbfe3c079d514ddc0022100d002b19049286e0201e847774bb99e7e18486e66b6b75dd0f4d6e494597b8c660121032487c2a32f7c8d57d2a93906a6457afd00697925b0e6e145d89af6d3bca33016ffffffff0436150000000000001976a914946cb2e08075bcbaf157e47bcb67eb2b2339d24288ac36150000000000001976a914bad510ef5889498cc9340ec00d71733440817b7788ac6c2a000000000000475121032487c2a32f7c8d57d2a93906a6457afd00697925b0e6e145d89af6d3bca330162102308673d16987eaa010e540901cc6fe3695e758c19f46ce604e174dac315e685a52ae4cc06800000000001976a9144b20c951517667dca9e651bf76d19747737bd6ea88ac00000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
        Assert.assertEquals(4, decodedTx.getVout().size());
        Assert.assertEquals(1, decodedTx.getVin().size());
        Assert.assertEquals(TxType.TX_MULTISIG.toString(), decodedTx.getVout().get(2).getType());

        // P2SH
        input = "010000000189632848f99722915727c5c75da8db2dbf194342a0429828f66ff88fab2af7d6000000008b483045022100abbc8a73fe2054480bda3f3281da2d0c51e2841391abd4c09f4f908a2034c18d02205bc9e4d68eafb918f3e9662338647a4419c0de1a650ab8983f1d216e2a31d8e30141046f55d7adeff6011c7eac294fe540c57830be80e9355c83869c9260a4b8bf4767a66bacbd70b804dc63d5beeb14180292ad7f3b083372b1d02d7a37dd97ff5c9effffffff0140420f000000000017a914f815b036d9bbbce5e9f2a00abd1bf3dc91e955108700000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
        Assert.assertEquals(TxType.TX_SCRIPTHASH.toString(), decodedTx.getVout().get(0).getType());
        Assert.assertEquals("3QJmV3qfvL9SuYo34YihAf3sRCW3qSinyC",
                decodedTx.getVout().get(0).getAddresses().get(0));

        // OP_RETURN
        input = "0100000001b63634c25f23018c18cbb24ad503672fe7c5edc3fef193ec0f581ddb27d4e4012d0000006a4730440220362b8ecd0570245657f8a4af4425a23eb32408cd7aaaafcc4d643acaf75334dd02205fbfed169d5338e0ea95b30d780230f9646e6869611e97bb9e8513471b5c830101210397ad4a7396e10298db9dc35a8456deafd6df6b43fc916fe84efa14b993212d1dffffffff010000000000000000216a1f59756b692077696c6c20796f75206d61727279206d65203f2054657473752e00000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
        Assert.assertEquals(1, decodedTx.getVout().size());
        Assert.assertEquals(TxType.TX_NULL_DATA.toString(), decodedTx.getVout().get(0).getType());
    }
    @Test
    public void testDecodeLongScript() {
        String input = "01000000010000000000000000000000000000000000000000000000000000000000000000fffffffffd01014cff01000000010000000000000000000000000000000000000000000000000000000000000000ffffffff6451515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151515151ffffffff020000000000000000015100f2052a010000001976a91479f4659ece305b4c5d0a3204dd7db6bff387878388ac0000000001000000010000000000000000000000000000000000000000000000000000000000000000ffffffff64515151515151515151515151515151515151ffffffff0100e1f505000000001976a914dd6e1b4d5799b0c17ee89ac3724563b44f7f47f588ac00000000";
        decodedTx.decode(input);
        System.out.println(JSON.toJSONString(decodedTx));
    }

} 
