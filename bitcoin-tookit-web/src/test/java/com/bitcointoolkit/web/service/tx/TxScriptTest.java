package com.bitcointoolkit.web.service.tx; 

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* TxScript Tester. 
* 
* @author <Authors name> 
* @since <pre>六月 17, 2018</pre> 
* @version 1.0 
*/ 
public class TxScriptTest { 

    @Before
    public void before() throws Exception { 
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: isPayToScriptHash() 
    */ 
    @Test
    public void testIsPayToScriptHash() throws Exception { 
    //TODO: Test goes here... 
    }

    @Test
    public void testJudgeType() {
        TxScript txScript = new TxScriptPubkey("76a9144a54e6d25321cc5447eabd61d54373022342ffc388ac");
        Assert.assertEquals(TxType.TX_PUBKEYHASH, txScript.judgeType());
    }

    @Test
    public void testPubkey() {// TODO
        TxScript txScript = new TxScriptPubkey("76a9144a54e6d25321cc5447eabd61d54373022342ffc388ac");
        Assert.assertEquals("OP_DUP OP_HASH160 4a54e6d25321cc5447eabd61d54373022342ffc3 OP_EQUALVERIFY OP_CHECKSIG",
                txScript.getScriptAsm());
    }

    @Test
    public void testScriptSig() {
        TxScript txScript = new TxScriptSig("47304402202c5e5424e83e44893eb172f0bdd193a2fd825787ad80d5515065ddbf6dd887a4022071335f4fd2736dce7a199b2e56ecf972e086e5d9410691dfb6bdfd3d22a16f0e4121026173240c95bd7e70c870b74419ee58c30f3edf47b3696ab8865c688291a89a9d");
        //TODO
    }

    @Test
    public void testGetOp() {
        TxScript txScript = new TxScriptPubkey("76a9144a54e6d25321cc5447eabd61d54373022342ffc388ac");
        boolean isPushOnly = txScript.isPushOnly(0);
        System.out.println(isPushOnly);
    }

    @Test
    public void testOpReturn() {
        TxScript txScript = new TxScriptPubkey("6a1f59756b692077696c6c20796f75206d61727279206d65203f2054657473752e");
        Assert.assertEquals(true, txScript.isUnspendable());
        Assert.assertEquals(TxType.TX_NULL_DATA, txScript.txType);
    }

    /**
     * 多重签名
     */
    @Test
    public void testRedeemScript() {
        // 2of3 multisig
        TxScript txScript = new TxScriptSig("52210356581d6b587dacd5dcadfdaa0ebb66461c17c5e535998edcdb5b978e6554cc7f2102179484b0b392f9fe7e4ca224a49dff45682188abb747cd015ce258a1a0cb01542102f72d493a66943035cbdce984e61a59cc1c60629c0ef66a3e4c2097982a4a752853ae");

        Assert.assertEquals("OP_2 0356581d6b587dacd5dcadfdaa0ebb66461c17c5e535998edcdb5b978e6554cc7f 02179484b0b392f9fe7e4ca224a49dff45682188abb747cd015ce258a1a0cb0154 02f72d493a66943035cbdce984e61a59cc1c60629c0ef66a3e4c2097982a4a7528 OP_3 OP_CHECKMULTISIG",
                txScript.getScriptAsm());
    }

} 
