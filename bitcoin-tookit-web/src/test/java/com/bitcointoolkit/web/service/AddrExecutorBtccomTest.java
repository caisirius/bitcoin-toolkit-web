package com.bitcointoolkit.web.service; 

import com.alibaba.fastjson.JSON;
import com.bitcointoolkit.web.support.Result;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* AddrExecutorBtccom Tester. 
* 
* @author <Authors name> 
* @since <pre>五月 29, 2018</pre> 
* @version 1.0 
*/ 
public class AddrExecutorBtccomTest {

    AddrExecutorBtccom addrExecutorBtccom;
    @Before
    public void before() throws Exception {
        addrExecutorBtccom = new AddrExecutorBtccom();
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: getUtxoOfLegacy(String legacy, int pageNum, int pageSize) 
    */ 
    @Test
    public void testGetUtxoOfLegacy() throws Exception {
        String legacy = "174uit4rSDN58K4MQX3vLjs8Y5FjXY2gCc";
        Result<UtxoResult> ret = addrExecutorBtccom.getUtxoOfLegacy(legacy,1,50);
        System.out.println(JSON.toJSONString(ret));
        Assert.assertTrue(ret.isSuccess());
    } 

    /** Method: getData() 
    */ 
    @Test
    public void testTransfer() throws Exception {
        String in = "{\"data\":{\"total_count\":2,\"page\":1,\"pagesize\":50,\"list\":[{\"tx_hash\":\"def5489240dfc14d27a776c8e05569f0b1abd94d070a0d15ae9cc4cccede8747\",\"tx_output_n\":0,\"tx_output_n2\":0,\"value\":2288063649,\"confirmations\":24580},{\"tx_hash\":\"9589d242049f7c205b88f6aaea055a59ea3b899ea50b4988a6553dfc40890294\",\"tx_output_n\":316,\"tx_output_n2\":0,\"value\":100000,\"confirmations\":16693}]},\"err_no\":0,\"err_msg\":null}";
        Result<UtxoResult> ret = addrExecutorBtccom.transfer(in);
        System.out.println(JSON.toJSONString(ret));
    }


} 
