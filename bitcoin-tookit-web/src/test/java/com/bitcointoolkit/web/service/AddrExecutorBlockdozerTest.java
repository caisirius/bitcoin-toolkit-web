package com.bitcointoolkit.web.service;

import com.alibaba.fastjson.JSON;
import com.bitcointoolkit.web.support.Result;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/** 
* AddrExecutorBlockdozer Tester. 
* 
* @author <Authors name> 
* @since <pre>五月 18, 2018</pre> 
* @version 1.0 
*/ 
public class AddrExecutorBlockdozerTest {

    AddrExecutorBlockdozer addrExecutorBlockdozer;
    @Before
    public void before() throws Exception {
        addrExecutorBlockdozer = new AddrExecutorBlockdozer();
    } 

    @After
    public void after() throws Exception { 
    } 

    /** Method: getUtxoOfLegacy(String legacy) 
    */ 
    @Test
    public void testGetUtxoOfLegacy() throws Exception {
        String legacy = "14Q9EfBp38WWJTS82HF2mpqsYg8RUnvfnp";
        Result<UtxoResult> ret = addrExecutorBlockdozer.getUtxoOfLegacy(legacy,1,50);
        System.out.println(JSON.toJSONString(ret));
        Assert.assertTrue(ret.isSuccess());
    }

} 
